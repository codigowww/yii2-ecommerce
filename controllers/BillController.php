<?php

namespace codigowww\yii2ecommerce\controllers;

use Yii;
use yii\helpers\Html;
use codigowww\yii2ecommerce\models\Bill;
use codigowww\yii2ecommerce\models\BillInfo;
use codigowww\yii2ecommerce\models\BillItem;
use codigowww\yii2ecommerce\models\PayMethod;
use codigowww\yii2ecommerce\components\Cart;
use codigowww\yii2ecommerce\models\BillSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use codigowww\yii2ecommerce\models\BillEvent;

/**
 * BillController implements the CRUD actions for Bill model.
 */
class BillController extends Controller {

    /**
     * @inheritdoc
     */
    public function beforeAction($action) {
        if ($action->id == 'ipn') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                    'pay' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'delete-multiple'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['checkout', 'pay', 'return', 'ipn'],
                        'allow' => true,
                    //'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    private function generateBillByCart($cart) {


        if ($cart->getCount(Cart::ITEM_PRODUCT) < 1) {

            return null;
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $bill = new Bill();
            $bill->currency = Yii::$app->getModule('ecommerce')->currency;
            $bill->status = 0;
            $bill->setCreatedAtTimestamp();
            if (!$bill->save()) {
                throw new \yii\base\ErrorException(Yii::t('ecommerce', 'Error when save bill'));
            }
            $bill->loadCart($cart);
            $bill->calculate();
            if (!$bill->save()) {
                throw new \yii\base\ErrorException(Yii::t('ecommerce', 'Error when save data bill'));
            }
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollBack();
            throw $e;
        }

        return $bill;
    }

    public function actionIpn($pm) {
        $paymethod = PayMethod::findOne($pm);


        if (is_null($paymethod))
            throw new \yii\base\ErrorException(Yii::t('ecommerce', 'Pay Method Not Found'));

        $paymethod_return = $paymethod->class::onIPN($paymethod);

        if (!isset($paymethod_return['hash']) || !isset($paymethod_return['status'])) {
            throw new \yii\base\ErrorException(Yii::t('ecommerce', 'Payment Method IPN Implementation Error'));
        }

        $pay_hash = $paymethod_return['hash'];
        $pay_status = $paymethod_return['status'];

        $bill = Bill::getByHash($pay_hash);
        if (is_null($bill))
            throw new \yii\base\ErrorException(Yii::t('ecommerce', 'Bill Not Found'));

        if (in_array($pay_status, Bill::getStatus())) {
            Yii::$app->response->statusCode = 200;
            $bill->status = $pay_status;
            if ($bill->status == Bill::STATUS_SUCCESS) {
                $bill->setPaymentDateTimestamp();
                if (isset($paymethod_return['transaction'])) {
                    $bill->transaction = $paymethod_return['transaction'];
                }
            }
            if ($bill->save()) {

                
                $billevent = new BillEvent();
                $billevent->bill = $bill;
                
                if ($bill->status == Bill::STATUS_SUCCESS) {
                    $bill->trigger(Bill::EVENT_BILL_SUCCESS,$billevent);
                } else if ($bill->status == Bill::STATUS_PENDING) {
                    $bill->trigger(Bill::EVENT_BILL_PENDING,$billevent);
                } else if ($bill->status == Bill::STATUS_FAIL) {
                    $bill->trigger(Bill::STATUS_FAIL,$billevent);
                }
            }
        }
    }

    public function actionReturn($hash, $status) {

        $bill = Bill::getByHash($hash);
        if (is_null($bill))
            throw new \yii\base\ErrorException(Yii::t('ecommerce', 'Bill Not Found'));

        $paymethod = $bill->paymentMethod;

        $paymethod->class::onReturn(Yii::$app->request->get(), $bill, $paymethod);

        return $this->render('return', compact('bill', 'paymethod', 'status'));
    }

    public function actionCheckout($id = null, $bi = null, $coupon = null) {

        $ecommerce = Yii::$app->getModule('ecommerce');
        $cart = $ecommerce->cart;
        $bill = null;


        $billinfo_list = [];
        $billinfo = BillInfo::findOne($bi);
        if (is_null($billinfo)) {
            $billinfo = new BillInfo();
        }
        if (!is_null($id)) {
            $bill = $this->findModel($id);

            if ($bill->status != Bill::STATUS_CREATED) {
                return $this->redirect(Url::toRoute(['/ecommerce/bill/view', 'id' => $id]));
            }
        }



        if (is_null($bill)) {
            $bill = $this->generateBillByCart($cart);
            if (is_null($bill)) {
                \Yii::$app->getSession()->setFlash('danger', 'Cart Empty');
                if (Yii::$app->request->isAjax)
                    return $this->renderAjax('_alert');
                return $this->render('_alert');
            }
            return $this->redirect(Url::toRoute(['/ecommerce/bill/checkout', 'id' => $bill->id]));
        }

        if (is_null($bill)) {
            throw new \yii\base\ErrorException(Yii::t('ecommerce', 'Bill null'));
        }

        if (!is_null($coupon)) {

            $c = \codigowww\yii2ecommerce\models\Coupon::findOne($coupon);
            if (!is_null($c) && $bill->coupon_id != $coupon) {
                $bill->coupon_id = $coupon;
                $bill->calculate();
                $bill->save();
            }
        }

        if (!Yii::$app->user->isGuest) {
            $billinfo_list = BillInfo::find()->where(['user_id' => Yii::$app->user->id])->all();
        }

        $billitems_dataProvider = new \yii\data\ArrayDataProvider(['allModels' => $bill->billItems,]);
        $pay_methods = PayMethod::find()->where(['status' => PayMethod::STATUS_ACTIVE])->all();
        $bill->scenario = "checkout";

        if (isset($pay_methods[0]) && !is_null($pay_methods[0])) {
            $bill->payment_method_id = $pay_methods[0]->id;
        }


        $returnVars = compact('bill', 'billitems_dataProvider', 'billinfo', 'billinfo_list', 'pay_methods');

        Yii::$app->getUser()->setReturnUrl(Url::toRoute(['/ecommerce/bill/checkout', 'id' => $id, 'bi' => $bi]));

        if (Yii::$app->request->isAjax)
            return $this->renderAjax('checkout', $returnVars);
        return $this->render('checkout', $returnVars);
    }

    public function actionPay($id) {
        $ecommerce = Yii::$app->getModule('ecommerce');
        $bill = $this->findModel($id);
        $bill_info_selected = Yii::$app->request->post('billinfo_id_selected');
        $billinfo = null;

        if ($bill_info_selected) {
            $billinfo = BillInfo::findOne($bill_info_selected);
        }

        if (is_null($billinfo)) {
            $billinfo = new BillInfo();
        }

        if ($bill->status != Bill::STATUS_CREATED) {
            return $this->redirect(Url::toRoute(['/ecommerce/bill/view', 'id' => $id]));
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {

            $saveBill = false;
            if ($ecommerce->billinfo) {
                if ($billinfo->load(Yii::$app->request->post())) {
                    $billinfo->setCreatedAtTimestamp();

                    if (!Yii::$app->user->isGuest) {
                        $billinfo->user_id = Yii::$app->user->id;
                    }

                    if ($billinfo->save()) {
                        $bill->bill_info_id = $billinfo->id;
                        $saveBill = true;
                    } else {
                        throw new \yii\base\ErrorException(Yii::t('ecommerce', 'Error when save bill info'));
                    }
                }
            }
            if ($bill->load(Yii::$app->request->post())) {
                $saveBill = true;
            }

            if ($saveBill) {
                if (!$bill->save()) {
                    throw new \yii\base\ErrorException(Yii::t('ecommerce', 'Error when save bill'));
                }
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }



        if (!is_null($bill->paymentMethod)) {
            $pay_method = $bill->paymentMethod;

            $redirect = $pay_method->class::sendBill($bill, $pay_method);
            return $this->redirect($redirect);
        } else {
            return $this->redirect(['checkout', 'id' => $bill->id, 'bi' => $billinfo->id]);
        }
    }

    /**
     * Lists all Bill models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new BillSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bill model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

        $print = false;
        $model = $this->findModel($id);
        $billitems_dataProvider = new \yii\data\ArrayDataProvider(['allModels' => $model->billItems,]);

        if (Yii::$app->request->get('print')) {
            $print = true;
        }


        $return_vars = ['model' => $model, 'billitems_dataProvider' => $billitems_dataProvider, 'print' => $print];
        if (Yii::$app->request->isAjax || $print) {
            return $this->renderAjax('view', $return_vars);
        }
        return $this->render('view', $return_vars);
    }

    /**
     * Creates a new Bill model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Bill();
        $billinfo = new BillInfo();


        if (Yii::$app->request->isPost) {

            if ($model->load(Yii::$app->request->post())) {
                $model->setCreatedAtTimestamp();
                if (!$model->save())
                    \Yii::$app->getSession()->setFlash('danger', Html::errorSummary($model));
            }

            if ($billinfo->load(Yii::$app->request->post())) {
                $billinfo->setCreatedAtTimestamp();
                if ($billinfo->save()) {
                    $model->bill_info_id = $billinfo->id;
                    $model->save();
                } else {
                    throw new \yii\base\ErrorException(Yii::t('ecommerce', 'Error when save bill info'));
                }
            }

            \Yii::$app->getSession()->setFlash('success', 'Los datos han sido guardados exitosamente.');
            return $this->redirect(['index']);
        }


        return $this->render('create', [
                    'model' => $model,
                    'billinfo' => $billinfo,
        ]);
    }

    public function actionDeleteMultiple() {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!$pk) {
            return;
        }
        return Bill::deleteAll(['id' => $pk]);
    }

    /**
     * Updates an existing Bill model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $billinfo = $model->billInfo;


        if ($model->load(Yii::$app->request->post())) {

            $model->setUpdatedAtTimestamp();
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Los datos han sido modificados exitosamente.');
                return $this->redirect(['index']);
            } else
                \Yii::$app->getSession()->setFlash('danger', Html::errorSummary($model));
        }


        return $this->render('update', [
                    'model' => $model,
                    'billinfo' => $billinfo,
        ]);
    }

    /**
     * Deletes an existing Bill model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {


        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax) {
            return Json::encode([
                        'success' => true,
            ]);
        } else
            return $this->redirect(['index']);
    }

    /**
     * Finds the Bill model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bill the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Bill::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La página solicitada no existe.');
        }
    }

}
