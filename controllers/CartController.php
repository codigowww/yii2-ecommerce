<?php

namespace codigowww\yii2ecommerce\controllers;

use Yii;
use yii\helpers\Html;
use codigowww\yii2ecommerce\models\Bill;
use codigowww\yii2ecommerce\models\BillInfo;
use codigowww\yii2ecommerce\models\BillItem;
use codigowww\yii2ecommerce\models\PayMethod;
use codigowww\yii2ecommerce\components\Cart;
use codigowww\yii2ecommerce\models\BillSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * BillController implements the CRUD actions for Bill model.
 */
class CartController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                //'pay' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'delete', 'add', 'delete-multiple'],
                        'allow' => true,
                    //'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Deletes an existing Bill model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {

        $cart = \Yii::$app->getModule('ecommerce')->get('cart');
        $cart->remove($id);
        return $this->render('index');
    }

    public function actionAdd() {
        $productid = Yii::$app->request->post('id');
        $type = Yii::$app->request->post('type');
        $cart = \Yii::$app->getModule('ecommerce')->get('cart');
        switch ($type) {

            case 'domain': $product = \common\models\Domain::findOne($productid);
                break;
            case 'host': $product = \common\models\Host::findOne($productid);
                break;
            default: throw new \yii\base\ErrorException(Yii::t('ecommerce', 'Type invalid'));
        }
        //      Yii::$app->session->destroy();
        // access the cart from "cart" subcomponent
        // Product is an AR model implementing CartProductInterface

        if (is_null($product))
            return 'error';
        // add an item to the cart
        
        $cart->add($product, 1,$product->hasQuantity());

        return 'ok';
    }

    /**
     * Lists all Bill models.
     * @return mixed
     */
    public function actionIndex() {
        
        if(Yii::$app->request->isAjax){
            return $this->renderAjax('index');
        }else{
            return $this->render('index');
        }
        
    }

    public function actionDeleteMultiple() {
        /* $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
          // Preventing extra unnecessary query
          if (!$pk) {
          return;
          }
          return Bill::deleteAll(['id' => $pk]); */
    }

}
