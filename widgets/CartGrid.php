<?php

namespace codigowww\yii2ecommerce\widgets;

use Yii;
use yii\base\Widget;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use codigowww\yii2ecommerce\components\Cart;

/**
 * Class Cart
 *
 * @package yii2mod\cart\widgets
 */
class CartGrid extends Widget {

    /**
     * @var \yii\data\BaseDataProvider
     */
    public $cartDataProvider;

    /**
     * @var array GridView columns
     */
    public $cartColumns = ['id', 'label'];

    /**
     * @var array GridView options
     */
    public $gridOptions = [];

    /**
     * @var string Only items of that type will be rendered. Defaults to Cart::ITEM_PRODUCT
     */
    public $itemType = Cart::ITEM_PRODUCT;

    /**
     * @inheritdoc
     */
    public function init() {
        $cart = Yii::$app->getModule('ecommerce')->get('cart');

        if (!isset($this->cartDataProvider)) {
            $models = [];

            $this->cartDataProvider = new ArrayDataProvider([
                'allModels' => ArrayHelper::getColumn($cart->getItems($this->itemType), 'item'),
                'pagination' => false,
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function run() {
        return $this->render('@vendor/codigowww/yii2-ecommerce/views/cart', [
                    'gridOptions' => $this->getGridOptions(),
        ]);
    }

    /**
     * Return grid options
     *
     * @return array
     */
    public function getGridOptions(): array {
        $cart = Yii::$app->getModule('ecommerce')->get('cart');
        return ArrayHelper::merge([
                    'columns' => [
                        [
                            'label' => Yii::t('ecommerce', 'Quantity'),
                            'value' => function($model) use ($cart) {

                                return $cart->getQuantity($model->getUniqueId());
                            },
                           
                        ],
                    ],
                        ], $this->gridOptions, [
                    'dataProvider' => $this->cartDataProvider,
                    'columns' => $this->cartColumns,
                        ], [
                    'columns' => [
                        [
                            'label' => '-',
                            'value' => function($model) use ($cart) {

                                return '<a href="' . Url::toRoute(['/ecommerce/cart/delete', 'id' => $model->id]) . '" data-pjax="1" class="btn btn-danger" data-method="POST"><i class="fas fa-trash"></i></a>';
                            },
                            'format' => 'raw',
                        ],
                    ]
                        ]
        );
    }

}
