Yii2 Ecommerce
==============
Ecommerce for yii2

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist codigowww/yii2-ecommerce "*"
```

or add

```
"codigowww/yii2-ecommerce": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :


```php
    'modules' => [
        'ecommerce' => [
            'class' => 'codigowww\yii2ecommerce\Module',

        ],
    ],
```

Full config:

```php
    'modules' => [
        'ecommerce' => [
            'class' => 'codigowww\yii2ecommerce\Module',
            'showLogin' => true,
            'controllerMap' => [
                'cart' => 'common\controllers\ecommerce\CartController',
            ],
            'viewPath' => '@common/views/ecommerce'
        ],
    ],
```

Routes:

/ecommerce/bill
/ecommerce/bill/ipn
/ecommerce/pay
/ecommerce/cart

Make sure acess to routes:

/ecommerce/bill/ipn