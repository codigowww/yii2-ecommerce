<?php

namespace codigowww\yii2ecommerce\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `ecommerce_bill_item`.
 */
class m190115_190554_create_ecommerce_bill_item_table extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('ecommerce_bill_item', [
            'id' => $this->primaryKey(),
            'bill_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'quantity' => $this->integer()->notNull(),
            'price' => $this->decimal(8,2)->notNull(),
            'details' => $this->text(),
            'status' => $this->integer(1)->notNull(),
        ]);



    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('ecommerce_bill_item');
    }

}
