<?php

namespace codigowww\yii2ecommerce\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `ecommerce_bill_info`.
 */
class m190115_193827_create_ecommerce_bill_info_table extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('ecommerce_bill_info', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'f_name' => $this->string(255)->notNull(),
            'l_name' => $this->string(255)->notNull(),
            'address' => $this->string(255),
            'address2' => $this->string(255),
            'city' => $this->string(255),
            'state' => $this->string(255),
            'zip' => $this->string(16),
            'tax_id' => $this->string(255),
            'bill_type_id' => $this->integer(),
            'country' => $this->string(255),
            'phone' => $this->string(255),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'status' => $this->integer(1),
        ]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('ecommerce_bill_info');
    }

}
