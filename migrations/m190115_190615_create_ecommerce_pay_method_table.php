<?php

namespace codigowww\yii2ecommerce\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `ecommerce_pay_method`.
 */
class m190115_190615_create_ecommerce_pay_method_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ecommerce_pay_method', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->unique()->notNull(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'config' => $this->text(),
            'class' => $this->string(255),
            'status' => $this->integer(1)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ecommerce_pay_method');
    }
}
