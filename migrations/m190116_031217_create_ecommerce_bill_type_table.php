<?php

namespace codigowww\yii2ecommerce\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `ecommerce_bill_type`.
 */
class m190116_031217_create_ecommerce_bill_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ecommerce_bill_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'status' => $this->integer(1)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ecommerce_bill_type');
    }
}
