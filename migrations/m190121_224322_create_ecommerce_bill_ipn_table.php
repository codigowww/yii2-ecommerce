<?php

namespace codigowww\yii2ecommerce\migrations;

use yii\db\Migration;

/**
 * Class m190121_224321_update_bill_info_table
 */
class m190121_224322_create_ecommerce_bill_ipn_table extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {

        $this->createTable('ecommerce_bill_ipn', [
            'id' => $this->primaryKey(),
            'bill_id' => $this->integer(),
            'payload' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'status' => $this->integer(1),
        ]);

        $this->addForeignKey(
                'fk-ecommerce_bill_ipn-bill_id', 'ecommerce_bill_ipn', 'bill_id', 'ecommerce_bill', 'id', 'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        
        $this->dropForeignKey('fk-ecommerce_bill_ipn-bill_id', 'ecommerce_bill_ipn');
        
        $this->dropTable('ecommerce_bill_ipn');
    }

}
