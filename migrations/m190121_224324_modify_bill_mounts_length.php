<?php

namespace codigowww\yii2ecommerce\migrations;

use yii\db\Migration;

/**
 * Class m190121_224321_update_bill_info_table
 */
class m190121_224324_modify_bill_mounts_length extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
		$this->alterColumn('ecommerce_bill', 'subtotal', $this->decimal(11,2));
		$this->alterColumn('ecommerce_bill', 'shipping', $this->decimal(11,2));
		$this->alterColumn('ecommerce_bill', 'tax', $this->decimal(11,2));
		$this->alterColumn('ecommerce_bill', 'total', $this->decimal(11,2));
		$this->alterColumn('ecommerce_bill', 'refund', $this->decimal(11,2));
		
		$this->alterColumn('ecommerce_coupon', 'discount', $this->decimal(11,2));
		
		$this->alterColumn('ecommerce_bill_item', 'price', $this->decimal(11,2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
       $this->alterColumn('ecommerce_bill', 'subtotal', $this->decimal(8,2));
	   $this->alterColumn('ecommerce_bill', 'shipping', $this->decimal(8,2));
	   $this->alterColumn('ecommerce_bill', 'tax', $this->decimal(8,2));
	   $this->alterColumn('ecommerce_bill', 'total', $this->decimal(8,2));
	   $this->alterColumn('ecommerce_bill', 'refund', $this->decimal(8,2));
	   
	   $this->alterColumn('ecommerce_coupon', 'discount', $this->decimal(8,2));
	   
	   $this->alterColumn('ecommerce_coupon', 'price', $this->decimal(8,2));
    }

}
