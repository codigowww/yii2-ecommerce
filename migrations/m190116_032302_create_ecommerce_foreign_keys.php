<?php

namespace codigowww\yii2ecommerce\migrations;

use yii\db\Migration;

/**
 * Class m190116_032302_create_ecommerce_foreign_keys
 */
class m190116_032302_create_ecommerce_foreign_keys extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {

        //bill table
        $this->addForeignKey(
                'fk-ecommerce_bill-coupon_id', 'ecommerce_bill', 'coupon_id', 'ecommerce_coupon', 'id', 'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
                'fk-ecommerce_bill-payment_method_id', 'ecommerce_bill', 'payment_method_id', 'ecommerce_pay_method', 'id', 'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
                'fk-ecommerce_bill-bill_info_id', 'ecommerce_bill', 'bill_info_id', 'ecommerce_bill_info', 'id', 'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
                'fk-ecommerce_bill-type_id', 'ecommerce_bill', 'type_id', 'ecommerce_bill_type', 'id', 'RESTRICT', 'CASCADE'
        );

        $this->createIndex(
                'idx-ecommerce_bill-user_id', 'ecommerce_bill', 'user_id'
        );
        
        $this->createIndex(
                'idx-ecommerce_bill-hash', 'ecommerce_bill', 'hash'
        );

        //bill item
        $this->addForeignKey(
                'fk-ecommerce_bill_item-bill_id', 'ecommerce_bill_item', 'bill_id', 'ecommerce_bill', 'id', 'CASCADE', 'CASCADE'
        );

        //bill info

        $this->addForeignKey(
                'fk-ecommerce_bill_info-bill_type_id', 'ecommerce_bill_info', 'bill_type_id', 'ecommerce_bill_type', 'id', 'RESTRICT', 'CASCADE'
        );


        $this->createIndex(
                'idx-ecommerce_bill_info-user_id', 'ecommerce_bill_info', 'user_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropForeignKey('fk-ecommerce_bill-coupon_id', 'ecommerce_bill');
        $this->dropForeignKey('fk-ecommerce_bill-payment_method_id', 'ecommerce_bill');
        $this->dropForeignKey('fk-ecommerce_bill-bill_info_id', 'ecommerce_bill');
        $this->dropForeignKey('fk-ecommerce_bill-type_id', 'ecommerce_bill');
        $this->dropIndex('idx-ecommerce_bill-user_id', 'ecommerce_bill');

        $this->dropForeignKey('fk-ecommerce_bill_item-bill_id', 'ecommerce_bill_item');

        $this->dropForeignKey('fk-ecommerce_bill_info-bill_type_id', 'ecommerce_bill_info');
        $this->dropIndex('idx-ecommerce_bill_info-user_id', 'ecommerce_bill_info');
    }

}
