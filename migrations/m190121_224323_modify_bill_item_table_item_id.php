<?php

namespace codigowww\yii2ecommerce\migrations;

use yii\db\Migration;

/**
 * Class m190121_224321_update_bill_info_table
 */
class m190121_224323_modify_bill_item_table_item_id extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('ecommerce_bill_item', 'ref_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('ecommerce_bill_item', 'ref_id');
    }

}
