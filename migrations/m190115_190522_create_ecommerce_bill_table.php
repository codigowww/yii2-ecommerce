<?php

namespace codigowww\yii2ecommerce\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `ecommerce_bill`.
 */
class m190115_190522_create_ecommerce_bill_table extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('ecommerce_bill', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'type_id' => $this->integer(),
            'coupon_id' => $this->integer(),
            'payment_method_id' => $this->integer(),
            'bill_info_id' => $this->integer(),
            'transaction' => $this->string(255),
            'hash' => $this->string(255),
            'currency' => $this->string(3),
            'subtotal' => $this->decimal(8,2),
            'shipping' => $this->decimal(8,2),
            'tax' => $this->decimal(8,2),
            'total' => $this->decimal(8,2),
            'refund' => $this->decimal(8,2),
            'shipping_info' => $this->text(),
            'note' => $this->text(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime(),
            'expire_at' => $this->dateTime(),
            'payment_date' => $this->dateTime(),
            'status' => $this->integer(1)->notNull(),
        ]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('ecommerce_bill');
    }

}
