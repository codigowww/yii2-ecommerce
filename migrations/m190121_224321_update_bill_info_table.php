<?php

namespace codigowww\yii2ecommerce\migrations;

use yii\db\Migration;

/**
 * Class m190121_224321_update_bill_info_table
 */
class m190121_224322_create_ecommerce_bill_ipn_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ecommerce_bill_info', 'email', $this->string(128));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('ecommerce_bill_info', 'email');
    }


}
