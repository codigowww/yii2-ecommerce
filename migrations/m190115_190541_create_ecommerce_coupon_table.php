<?php

namespace codigowww\yii2ecommerce\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `ecommerce_coupon`.
 */
class m190115_190541_create_ecommerce_coupon_table extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('ecommerce_coupon', [
            'id' => $this->primaryKey(),
            'code' => $this->string(50)->unique(),
            'quantity' => $this->integer(),
            'reuse' => $this->integer(1),
            'status' => $this->integer(1),
            'currency' => $this->string(3),
            'discount' => $this->decimal(8,2),
            'discount_type' => $this->integer(1),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'begin_at' => $this->dateTime(),
            'end_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('ecommerce_coupon');
    }

}
