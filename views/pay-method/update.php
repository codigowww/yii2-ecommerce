<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model codigowww\yii2ecommerce\models\PayMethod */

$this->title = Yii::t('ecommerce', 'Modificar {modelClass}: ', [
    'modelClass' => 'Pay Method',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('ecommerce', 'Pay Methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('ecommerce', 'Modificar');
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="pay-method-modificar">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>


</div>