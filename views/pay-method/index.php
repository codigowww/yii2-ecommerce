<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel codigowww\yii2ecommerce\models\PayMethodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('ecommerce', 'Pay Methods');
$this->params['breadcrumbs'][] = $this->title;

\dominus77\sweetalert2\assets\SweetAlert2Asset::register($this);


?>
<div class="pay-method-index">

   <!-- <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php

    $toolbar =  Html::a('<i class="fas fa-plus"></i> Nuevo ', ['create'], ['class' => 'btn btn-success']). 
                Html::a('<i class="fas fa-trash"></i>', 'javascript:void(0)' , ['id' => 'btn-bulk-del', 'class' => 'btn btn-danger', 'title'=>'Borrar Seleccionados']) . 
                Html::a('<i class="fas fa-redo-alt"></i>', ['index'], ['class'=>'btn btn-dark', 'title'=>'Actualizar']);?>

    <?= GridView::widget([
        'options' => ['id' => 'grid-name'],
        'dataProvider' => $dataProvider,
        'toolbar'=> [
                  ['content'=>  $toolbar],
                  //'{export}',
                  '{toggleData}',
        ],
            'panel' => [
                'type' => kartik\grid\GridView::TYPE_PRIMARY,
                'heading' => $this->title,
                'headingOptions' => [
                    'class' => 'card-header text-white bg-primary d-flex align-items-center justify-content-between'
                ],
            ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\CheckboxColumn'],
            //['class' => 'kartik\grid\SerialColumn'],

                        'id',
            'name',
            'title',
            'description:ntext',
            'status',

            
            [
                'class' => '\kartik\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'width' => '95px',
                'noWrap' => true,
            ], 
        ],
    ]); ?>
</div>


<div class="modal fade modal-scroll" id="ajax" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>



<?=    $this->registerJs(' 
    $(document).ready(function(){
      $("#btn-bulk-del").click(function(){
      
      
      Swal({
        title: "'.Yii::t('app','¿Seguro?').'",
        text: "'.Yii::t('app','¿Está seguro de borrar estos items?').'",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "'.Yii::t('app','Si Borrar').'",
        cancelButtonText: "'.Yii::t('app','Cancelar').'"
      }).then((result) => {
      

        
        
        if (result.value) {
        
                      $.post(
            "delete-multiple", 
            {
                pk : $("#grid-name").yiiGridView("getSelectedRows")
            },
            function () {
                $.pjax.reload({container:"#grid-name"});
            }
        );
        
       
        }
})
      

      });
    });', \yii\web\View::POS_READY);
?>