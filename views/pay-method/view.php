<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model codigowww\yii2ecommerce\models\PayMethod */

$this->title = $model->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('ecommerce', 'Pay Methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name];
?>



<div class="row">
    <div class="col-12">
        <div class="card">

            <div class="card-header bg-primary text-white">

                <h3 class="card-title"> <?=    $this->title  ?> </h3>
            </div>
            <div class="card-body">
                <div class="pay-method-view">


                    <?= DetailView::widget([
                    'condensed'=>true,
                    'hover'=>true,
                    'mode'=>DetailView::MODE_VIEW,
                    'model' => $model,
                    'responsive' => true,
                    'vAlign' => 'center',
                    'attributes' => [
                                'id',
            'name',
            'title',
            'description:ntext',
            'status',
                    ],
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</div>

