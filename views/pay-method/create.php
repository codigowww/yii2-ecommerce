<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model codigowww\yii2ecommerce\models\PayMethod */

$this->title = Yii::t('ecommerce', 'Crear Pay Method');
$this->params['breadcrumbs'][] = ['label' => Yii::t('ecommerce', 'Pay Methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="pay-method-crear">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>