<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model codigowww\yii2ecommerce\models\Bill */

$this->title = Yii::t('ecommerce', 'Crear Bill');
$this->params['breadcrumbs'][] = ['label' => Yii::t('ecommerce', 'Bills'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['page-header-showtitle'] = false;
?>
<div class="bill-crear">

<?=
$this->render('_form', [
    'model' => $model,
    'billinfo' => $billinfo,
])
?>

</div>