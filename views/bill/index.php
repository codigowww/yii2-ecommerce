<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use codigowww\yii2ecommerce\models\PayMethod;
use codigowww\yii2ecommerce\models\Bill;

/* @var $this yii\web\View */
/* @var $searchModel codigowww\yii2ecommerce\models\BillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('ecommerce', 'Bills');
$this->params['breadcrumbs'][] = $this->title;

\dominus77\sweetalert2\assets\SweetAlert2Asset::register($this);
?>
<div class="bill-index">

   <!-- <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <?php
    $toolbar = Html::a('<i class="fas fa-plus"></i> Nuevo ', ['create'], ['class' => 'btn btn-success']) .
            Html::a('<i class="fas fa-trash"></i>', 'javascript:void(0)', ['id' => 'btn-bulk-del', 'class' => 'btn btn-danger', 'title' => 'Borrar Seleccionados']) .
            Html::a('<i class="fas fa-redo-alt"></i>', ['index'], ['class' => 'btn btn-dark', 'title' => 'Actualizar']);
    ?>

    <?=
    GridView::widget([
        'options' => ['id' => 'grid-id'],
        'dataProvider' => $dataProvider,
        'toolbar' => [
            ['content' => $toolbar],
            //'{export}',
            '{toggleData}',
        ],
        'panel' => [
            'type' => kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => $this->title,
            'headingOptions' => [
                'class' => 'card-header text-white bg-primary d-flex align-items-center justify-content-between'
            ],
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\CheckboxColumn'],
            //['class' => 'kartik\grid\SerialColumn'],
            [
              'attribute' => 'id',
               'width' => '5%',
                
            ],
            'created_at',
            
 
            //'user_id',
            'type_id',
            //'coupon_id',
            [
                'class' => 'kartik\grid\EnumColumn',
                'attribute' => 'payment_method_id',
                'enum' => PayMethod::getArray(),
                'filter' => PayMethod::getArray(),
            ],
            // 'bill_info_id',
            // 'transaction',
            // 'currency',
            // 'subtotal',
            // 'shipping',
            // 'tax',
            [
                'class' => 'kartik\grid\EnumColumn',
                'attribute' => 'status',
                'enum' => [
                    Bill::STATUS_CREATED => '<span class="badge badge-secondary">' . Yii::t('ecommerce', 'Created') . '</span>',
                    Bill::STATUS_PENDING => '<span class="badge badge-info">' . Yii::t('ecommerce', 'Pending') . '</span>',
                    Bill::STATUS_SUCCESS => '<span class="badge badge-success">' . Yii::t('ecommerce', 'Success') . '</span>',
                    Bill::STATUS_FAIL => '<span class="badge badge-danger">' . Yii::t('ecommerce', 'Fail') . '</span>',
                ],
                'filter' => [// will override the grid column filter (i.e. `loadEnumAsFilter` will be parsed as `false`)
                    Bill::STATUS_CREATED => Yii::t('ecommerce', 'Created'),
                    Bill::STATUS_PENDING => Yii::t('ecommerce', 'Pending'),
                    Bill::STATUS_SUCCESS => Yii::t('ecommerce', 'Success'),
                    Bill::STATUS_FAIL => Yii::t('ecommerce', 'Fail'),
                ],
                'format' => 'raw',
            ],
            'total',
            // 'refund',
            // 'shipping_info:ntext',
            // 'note:ntext',
            // 'updated_at',
            // 'payment_date',
            [
                'class' => '\kartik\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'width' => '95px',
                'noWrap' => true,
            ],
        ],
    ]);
    ?>
</div>


<div class="modal fade modal-scroll" id="ajax" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>



<?= $this->registerJs(' 
    $(document).ready(function(){
      $("#btn-bulk-del").click(function(){
      
      
      Swal({
        title: "' . Yii::t('app', '¿Seguro?') . '",
        text: "' . Yii::t('app', '¿Está seguro de borrar estos items?') . '",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "' . Yii::t('app', 'Si Borrar') . '",
        cancelButtonText: "' . Yii::t('app', 'Cancelar') . '"
      }).then((result) => {
      

        
        
        if (result.value) {
        
                      $.post(
            "delete-multiple", 
            {
                pk : $("#grid-id").yiiGridView("getSelectedRows")
            },
            function () {
                $.pjax.reload({container:"#grid-id"});
            }
        );
        
       
        }
})
      

      });
    });', \yii\web\View::POS_READY);
?>