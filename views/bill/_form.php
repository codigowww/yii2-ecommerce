<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use codigowww\yii2ecommerce\models\Bill;
use codigowww\yii2ecommerce\models\BillType;
use codigowww\yii2ecommerce\models\PayMethod;
use codigowww\yii2ecommerce\models\Coupon;
use kartik\select2\Select2;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model codigowww\yii2ecommerce\models\Bill */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bill-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-6">

            <?= $this->render('bill-info', ['model' => $billinfo, 'form' => $form]) ?>

        </div>
        <div class="col-6">
             <?= $this->render('bill-items', ['model' => $model,'form' => $form]) ?>
        </div>
        
    </div>
    <div class="row">
        <div class="col-12">

            <div class="card mt-3">
                <div class="card-header bg-primary text-white"><h3 class="card-title"><i class="fas fa-edit"></i><?= Html::encode($this->title) ?></h3></div>

                <div class="card-body">


                    <div class="row">
                        <div class="col-12 col-md-4">
                            <?= $form->field($model, 'user_id')->textInput() ?>
                        </div>
                        <div class="col-12 col-md-4">
                            <?=
                            $form->field($model, 'type_id')->widget(Select2::classname(), [
                                'data' => BillType::getArray(),
                                'options' => ['placeHolder' => ''],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-12 col-md-4">
                            <?=
                            $form->field($model, 'status')->widget(Select2::classname(), [
                                'data' => [
                                    Bill::STATUS_CREATED => Yii::t('ecommerce', 'Created'),
                                    Bill::STATUS_PENDING => Yii::t('ecommerce', 'Pending'),
                                    Bill::STATUS_SUCCESS => Yii::t('ecommerce', 'Success'),
                                    Bill::STATUS_FAIL => Yii::t('ecommerce', 'Fail'),
                                ],
                                'options' => ['placeholder' => ''],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <?=
                            $form->field($model, 'payment_method_id')->widget(Select2::classname(), [
                                'data' => PayMethod::getArray(),
                                'options' => ['placeholder' => ''],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>


                        </div>
                        <div class="col-12 col-md-4">
                            <?= $form->field($model, 'payment_date')->textInput() ?>


                        </div>
                        <div class="col-12 col-md-4">
                            <?= $form->field($model, 'transaction')->textInput(['maxlength' => true]) ?>
                        </div>

                    </div>













                    <div class="row">
                        <div class="col-6">

                            <?= $form->field($model, 'shipping_info')->textarea(['rows' => 6]) ?>
                            <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>
                        </div>
                        <div class="col-6">
                            <?=
                            $form->field($model, 'subtotal')->widget(NumberControl::classname(), [
                                'maskedInputOptions' => [
                                    'prefix' => '$ ',
                                    'suffix' => '',
                                    'allowMinus' => false
                                ],
                                'options' => [
                                    'type' => 'hidden',
                                    'class' => 'kv-saved',
                                    'readonly' => true,
                                    'tabindex' => 1000
                                ],
                                'displayOptions' => ['class' => 'form-control kv-monospace'],
                                'saveInputContainer' => ['class' => 'kv-saved-cont']
                            ]);
                            ?>
                            <?=
                            $form->field($model, 'shipping')->widget(NumberControl::classname(), [
                                'maskedInputOptions' => [
                                    'prefix' => '$ ',
                                    'suffix' => '',
                                    'allowMinus' => false
                                ],
                                'options' => [
                                    'type' => 'hidden',
                                    'label' => '<label>Saved Value: </label>',
                                    'readonly' => true,
                                    'tabindex' => 1000
                                ],
                                'displayOptions' => ['class' => 'form-control kv-monospace'],
                                'saveInputContainer' => ['class' => 'kv-saved-cont']
                            ]);
                            ?>

                            <?=
                            $form->field($model, 'coupon_id')->widget(Select2::classname(), [
                                'data' => Coupon::getArray(),
                                'options' => ['placeholder' => ''],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                            <?=
                            $form->field($model, 'tax')->widget(NumberControl::classname(), [
                                'maskedInputOptions' => [
                                    'prefix' => '$ ',
                                    'suffix' => '',
                                    'allowMinus' => false
                                ],
                                'options' => [
                                    'type' => 'hidden',
                                    'class' => 'kv-saved',
                                    'readonly' => true,
                                    'tabindex' => 1000
                                ],
                                'displayOptions' => ['class' => 'form-control kv-monospace'],
                                'saveInputContainer' => ['class' => 'kv-saved-cont']
                            ]);
                            ?>
                            <?=
                            $form->field($model, 'total')->widget(NumberControl::classname(), [
                                'maskedInputOptions' => [
                                    'prefix' => '$ ',
                                    'suffix' => '',
                                    'allowMinus' => false
                                ],
                                'options' => [
                                    'type' => 'hidden',
                                    'class' => 'kv-saved',
                                    'readonly' => true,
                                    'tabindex' => 1000
                                ],
                                'displayOptions' => ['class' => 'form-control kv-monospace'],
                                'saveInputContainer' => ['class' => 'kv-saved-cont']
                            ]);
                            ?>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">

                        </div>
                        <div class="col-6">
                            <?=
                            $form->field($model, 'refund')->widget(NumberControl::classname(), [
                                'maskedInputOptions' => [
                                    'prefix' => '$ ',
                                    'suffix' => '',
                                    'allowMinus' => false
                                ],
                                'options' => [
                                    'type' => 'hidden',
                                    'class' => 'kv-saved',
                                    'readonly' => true,
                                    'tabindex' => 1000
                                ],
                                'displayOptions' => ['class' => 'form-control kv-monospace'],
                                'saveInputContainer' => ['class' => 'kv-saved-cont']
                            ]);
                            ?>
                        </div>
                    </div>












                </div>

            </div>


        </div>
    </div>



    <div class="mt-3 text-center">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> ' . Yii::t('ecommerce', 'Guardar') : '<i class="fas fa-save"></i> ' . Yii::t('ecommerce', 'Modificar'), ['class' => $model->isNewRecord ? 'btn btn-success btn-lg px-5' : 'btn btn-success btn-lg px-5']) ?>
    </div>     

    <?php ActiveForm::end(); ?>

</div>
