<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model codigowww\yii2ecommerce\models\Bill */

$this->title = $model->id;

$this->params['breadcrumbs'][] = ['label' => Yii::t('ecommerce', 'Bills'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id];
?>



<div class="card">
    <div class="card-header">
        <h3 class="card-title d-inline-block mr-3"> <?= Yii::t('ecommerce', 'Invoice') . ' ' . $this->title ?></h3>
        <strong><?= $model->created_at ?></strong>
        <span class="float-right"> <strong><?= Yii::t('ecommerce', 'Status') ?>:</strong> <?= $model->statusText ?></span>

    </div>
    <div class="card-body">
        <div class="row mb-4">
            <div class="col-sm-6">
                <h6 class="mb-3"><?= Yii::t('ecommerce', 'From') ?>:</h6>
                <div>
                    <strong>Webz Poland</strong>
                </div>
                <div>Madalinskiego 8</div>
                <div>71-101 Szczecin, Poland</div>
                <div>Email: info@webz.com.pl</div>
                <div>Phone: +48 444 666 3333</div>
            </div>

            <div class="col-sm-6">
                <h6 class="mb-3"><?= Yii::t('ecommerce', 'To') ?>:</h6>
                <?php if ($model->billInfo): ?>
                    <div>
                        <strong><?= $model->billInfo->f_name . ' ' . $model->billInfo->l_name ?></strong>
                    </div>
                    <div>Attn: Daniel Marek</div>
                    <div><?= $model->billInfo->address ?></div>
                    <?= $model->billInfo->address2 ? '<div>' . $model->billInfo->address . '</div>' : '' ?>
                    <div>Email: <?= $model->billInfo->email ?></div>
                    <div>Phone: <?= $model->billInfo->phone ?></div>
                <?php endif; ?>
            </div>



        </div>
        <div class="row">
            <div class="col-6">
                <?= $model->note ?>
            </div>
            <div class="col-6">
                <?= $model->shipping_info ?>
            </div>
        </div>
        <?= $this->render('invoice',['model' => $model,'billitems_dataProvider' => $billitems_dataProvider])?>

    </div>
</div>


<div class="text-center mt-3">
    <a class="btn btn-info" href="<?= Url::toRoute(['/ecommerce/bill/view','id' => $model->id,'print' => true]) ?>" ><i class="fas fa-print"></i> <?= Yii::t('ecommerce', 'Print') ?></a>
</div>

<?php if (isset($print) && $print): ?>
    <script>
        window.print();
    </script>
<?php endif; ?>