<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model codigowww\yii2ecommerce\models\Bill */

$this->title = Yii::t('ecommerce', 'Modificar {modelClass}: ', [
            'modelClass' => 'Bill',
        ]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('ecommerce', 'Bills'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('ecommerce', 'Modificar');
Yii::$app->params['page-header-showtitle'] = false;
?>
<div class="bill-modificar">

    <?=
    $this->render('_form', [
        'model' => $model,
        'billinfo' => $billinfo,
    ])
    ?>


</div>