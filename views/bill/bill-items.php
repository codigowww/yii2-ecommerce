<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use kartik\builder\TabularForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

$billitems = $model->billItems; // where `id` is your primary key
/* $dataProvider = new ActiveDataProvider([
  'query' => $query,
  ]); */
?>

<div class="card">

    <div class="card-header bg-primary text-white"><h3 class="card-title"><?= Yii::t('ecommerce', 'Bill Items') ?></h3></div>
    <div class="card-body">



        <div class="row mb-2">
            <div class="col-3"><?= Yii::t('ecommerce', 'Name') ?></div>
            <div class="col-3"><?= Yii::t('ecommerce', 'Price') ?></div>
            <div class="col-3"><?= Yii::t('ecommerce', 'Quantity') ?></div>
            <div class="col-3"><?= Yii::t('ecommerce', 'Action') ?></div>
        </div>

        <div class="bill-items">

            <?php
            $i = 0;
            foreach ($billitems as $billitem):
                ?>

                <div class="row">
                    <input type="hidden" name="BillItem[<?= $i ?>][id]" value="<?= $billitem->id ?>">
                    <input type="hidden" name="BillItem[<?= $i ?>][remove]" class="remove-input" value="0">
                    <div class="col-3"><input type="text" name="BillItem[<?= $i ?>][name]" class="form-control" value="<?= $billitem->name ?>"></div>
                    <div class="col-3"><input type="number" name="BillItem[<?= $i ?>][price]" class="form-control" value="<?= $billitem->price ?>"></div>
                    <div class="col-3"><input type="text" name="BillItem[<?= $i ?>][quantity]" class="form-control" value="<?= $billitem->quantity ?>"></div>
                    <div class="col-3"><button type="button" class="btn btn-danger btn-remove-item"><i class="fas fa-trash"></i></button></div>
                </div>
                <?php
                $i++;
            endforeach;
            ?>

            <input type="hidden" class="items-count" name="items-count" value="<?= $i - 1 ?>">
        </div>
        <button class="btn btn-success btn-block btn-new-item mt-3" type="button"><?= Yii::t('ecommerce', 'Add Item'); ?></button>

        <div class="templateitem d-none">
            <div class="row mt-1">
                <input type="hidden" name="BillItem[{nrow}][id]" value="">
                <input type="hidden" name="BillItem[{nrow}][remove]" class="remove-input" value="0">
                <div class="col-3"><input type="text" name="BillItem[{nrow}][name]" class="form-control" value=""></div>
                <div class="col-3"><input type="number" name="BillItem[{nrow}][price]" class="form-control" value=""></div>
                <div class="col-3"><input type="text" name="BillItem[{nrow}][quantity]" class="form-control" value=""></div>
                <div class="col-3"><button type="button" class="btn btn-danger btn-remove-item"><i class="fas fa-trash"></i></button></div>
            </div>
        </div>

    </div>

</div>



<?php
$this->registerJs(
        <<<JS
        
        $(".btn-new-item").click(function(){
            var templateitem = $('.templateitem');
            var itemscount = parseInt($('.items-count').val()) + 1;
            var elementappend = templateitem.html().replace(/{nrow}/g, itemscount )
            $('.bill-items').append(elementappend);
             $('.items-count').val(itemscount);
        });

        $(".bill-items").on('click','.btn-remove-item',function(){
            var removeinput = $(this).siblings(".remove-input");
            removeinput.val(1);
            $(this).closest(".row").addClass("d-none");
        });
JS
);
?>

