<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?=
kartik\grid\GridView::widget([
    'dataProvider' => $billitems_dataProvider,
    'panel' => false,
    'layout' => '{items}',
    'columns' => [
        'quantity',
        'name',
        'details',
        'price',
        [
            'class' => '\kartik\grid\FormulaColumn',
            'label' => Yii::t('ecommerce', 'SubTotal'),
            'value' => function ($model, $key, $index, $widget) {
                return $model->price * $model->quantity;
            }
        ],
    ],
]);
?>

<div class="row">
    <div class="col-lg-4 col-sm-5">

    </div>

    <div class="col-lg-4 col-sm-5 ml-auto">
        <table class="table table-clear">
            <tbody>
                <tr>
                    <td class="left">
                        <strong><?= Yii::t('ecommerce', 'SubTotal') ?></strong>
                    </td>
                    <td class="right"><?= $model->subtotal ?></td>
                </tr>

<?php if ($model->coupon): ?>
                    <tr>
                        <td class="left">
                            <strong><?= Yii::t('ecommerce', 'Discount') ?> <?= $model->coupon->code ?> </strong>
                        </td>
                        <td class="right"><?= $model->coupon->discount_type == Coupon::TYPE_PERCENT ? $model->coupon->discount . '%' : '-' . $model->coupon->discount ?></td>
                    </tr>
<?php endif; ?>
                <?php if ($model->shipping): ?>
                    <tr>
                        <td class="left">
                            <strong><?= Yii::t('ecommerce', 'Shipping') ?></strong>
                        </td>
                        <td class="right"><?= $model->shipping ?></td>
                    </tr>
<?php endif; ?>
                <?php if ($model->tax): ?>
                    <tr>
                        <td class="left">
                            <strong><?= Yii::t('ecommerce', 'Tax') ?></strong>
                        </td>
                        <td class="right"><?= $model->tax ?></td>
                    </tr>
<?php endif; ?>
                <tr>
                    <td class="left">
                        <strong><?= Yii::t('ecommerce', 'Total') ?></strong>
                    </td>
                    <td class="right">
                        <strong><?= $model->total ?></strong>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>

</div>