<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model codigowww\yii2ecommerce\models\BillInfo */
/* @var $form yii\widgets\ActiveForm */
//if (Yii::$app->user->isGuest || (!$model->isNewRecord && !Yii::$app->user->isGuest) ):


$notdisplaybillinfo = !Yii::$app->user->isGuest && $model->isNewRecord && (Yii::$app->request->get('bi') == null || Yii::$app->request->get('bi') > 0) && !empty($billinfo_list);
?> 



<?php if ($notdisplaybillinfo ): ?>
<h4><?= Yii::t('ecommerce', 'Select one preloaded bill info') ?></h4>
<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
    <?php foreach ($billinfo_list as $billinfo): ?>
        <a class="btn btn-dark btn-block btn-lg py-3"  href="<?= Url::toRoute(['/ecommerce/bill/checkout', 'id' => $bill->id, 'bi' => $billinfo->id]) ?>" ><?= $billinfo->f_name . ' ' . $billinfo->l_name ?></a>
    <?php endforeach; ?>
    <a class="btn btn-success btn-block btn-lg py-3"  href="<?= Url::toRoute(['/ecommerce/bill/checkout', 'id' => $bill->id, 'bi' => 0]) ?>" ><?= Yii::t('ecommerce', 'New Bill Info') ?></a>

</div>

<?php endif; ?>

<div class="bill-info-form <?= $notdisplaybillinfo ? 'd-none' : ''?>"> 
    <h4><?= Yii::t('ecommerce', 'Please complete the billing info:') ?></h4>

    <?= Html::hiddenInput('billinfo_id_selected', Yii::$app->request->get('bi') ); ?>
    
    <div class="row">
        <div class="col-12 col-md-4"><?= $form->field($model, 'f_name')->textInput(['maxlength' => true, 'readOnly' => !$model->isNewRecord ]) ?></div>
        <div class="col-12 col-md-4"><?= $form->field($model, 'l_name')->textInput(['maxlength' => true, 'readOnly' => !$model->isNewRecord]) ?></div>
        <div class="col-12 col-md-4"><?= $form->field($model, 'tax_id')->textInput(['maxlength' => true, 'readOnly' => !$model->isNewRecord]) ?></div>
    </div>

    <div class="row">
        <div class="col-12 col-md-6">            
            <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'readOnly' => !$model->isNewRecord]) ?>
            <?= $form->field($model, 'address2')->textInput(['maxlength' => true, 'readOnly' => !$model->isNewRecord]) ?>
        </div>
        <div class="col-12 col-md-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'readOnly' => !$model->isNewRecord]) ?>
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'readOnly' => !$model->isNewRecord]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6 col-md-2"><?= $form->field($model, 'zip')->textInput(['maxlength' => true, 'readOnly' => !$model->isNewRecord]) ?></div>
        <div class="col-6 col-md-3"><?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readOnly' => !$model->isNewRecord]) ?></div>
        <div class="col-6 col-md-3"><?= $form->field($model, 'state')->textInput(['maxlength' => true, 'readOnly' => !$model->isNewRecord]) ?></div>
        <div class="col-6 col-md-4"><?= $form->field($model, 'country')->textInput(['maxlength' => true, 'readOnly' => !$model->isNewRecord]) ?></div>

    </div>





</div> 




