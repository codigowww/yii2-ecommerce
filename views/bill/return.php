<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

switch ($status) {
    case 'success': $this->title = Yii::t('ecommerce', 'Thank for you buy!');
        break;
    case 'pending': $this->title = Yii::t('ecommerce', 'Purcharse Pending!');
        break;
    case 'fail': $this->title = Yii::t('ecommerce', 'Payment Fail!');
        break;
}
?>

<div class="card">
    <div class="card-header bg-primary text-white">
        <h3 class="card-title"><?= $this->title ?></h3>
    </div>

    <div class="card-body">
        <a href="<?= Url::toRoute(['/site/index']) ?>" class="btn btn-info">
            <?= Yii::t('ecommerce', 'Go to Home') ?>
        </a>
    </div>

</div>