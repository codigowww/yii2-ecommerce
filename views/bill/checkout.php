<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = "Check Out";

?>



<?php $form = ActiveForm::begin(['method' => 'POST', 'action' => Url::toRoute(['/ecommerce/bill/pay', 'id' => $bill->id])]); ?> 


<div class="row">

    <div class="col-12 col-md-6">
        <div class="card"> 
            <div class="card-header bg-primary text-white"><h3 class="card-title"><?= Yii::t('ecommerce', 'Bill Info') ?></h3></div> 

            <div class="card-body"> 

       
                <?php if (Yii::$app->user->isGuest && Yii::$app->getModule('ecommerce')->showLogin): ?>
                    <div class="row mb-3">
                        <div class="col-6 pr-2"><div class="card"><div class="card-body"><h4><?= Yii::t('ecommerce', 'Have a account?') ?></h4> <a href="<?= Url::to('/login') ?>" class="btn btn-info btn-block btn-lg"><?= Yii::t('ecommerce', 'Login') ?></a> </div> </div></div>
                        <div class="col-6 pl-2"><div class="card"><div class="card-body"> <h4><?= Yii::t('ecommerce', 'Or Register?') ?></h4> <a href="<?= Url::to('/register') ?>" class="btn btn-success btn-block btn-lg"><?= Yii::t('ecommerce', 'Register') ?></a></div> </div></div>
                    </div>

                <?php endif; ?>
                <?= $this->render('bill-info', ['model' => $billinfo, 'bill' => $bill, 'form' => $form, 'billinfo_list' => $billinfo_list]) ?>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-header bg-primary text-white"><h3 class="card-title"><?= Yii::t('ecommerce', 'Invoice') ?></h3></div>

            <div class="card-body">
                <?= $this->render('invoice', ['model' => $bill, 'billitems_dataProvider' => $billitems_dataProvider]) ?>
            </div>
        </div>
        <?php if ($bill->total > 0): ?>
            <div class="card mt-3">
                <div class="card-header bg-primary text-white"><h3 class="card-title"><?= Yii::t('ecommerce', 'Pay Method') ?></h3></div>
                <div class="card-body">


                    <?= $form->field($bill, 'payment_method_id')->radioList(ArrayHelper::map($pay_methods, 'id', 'name'))->label(false); ?>

                </div>
            </div>

        <?php endif; ?>

    </div>


</div>



<div class="mt-3 text-center"> 
    <?= Html::submitButton(Yii::t('ecommerce', 'PAY'), ['class' => 'btn btn-success btn-lg px-5']) ?> 
</div>      

<?php ActiveForm::end(); ?> 