<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php

echo \codigowww\yii2ecommerce\widgets\CartGrid::widget([
    'cartColumns' => [
        [
            'attribute' => 'label',
            'label' => Yii::t('ecommerce', 'label'),
        ],
        [
            'attribute' => 'price',
            'label' => Yii::t('ecommerce', 'price'),
        ],

    ]
]);
?>