<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model codigowww\yii2ecommerce\models\BillType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bill-type-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card">
        <div class="card-header bg-primary text-white"><h3 class="card-title"><i class="fas fa-edit"></i><?= Html::encode($this->title) ?></h3></div>

        <div class="card-body">



                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>






        </div>

    </div>
    
    <div class="mt-3 text-center">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> '.Yii::t('ecommerce', 'Guardar') : '<i class="fas fa-save"></i> '.Yii::t('ecommerce', 'Modificar'), ['class' => $model->isNewRecord ? 'btn btn-success btn-lg px-5' : 'btn btn-success btn-lg px-5']) ?>
    </div>     

    <?php ActiveForm::end(); ?>

</div>
