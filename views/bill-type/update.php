<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model codigowww\yii2ecommerce\models\BillType */

$this->title = Yii::t('ecommerce', 'Modificar {modelClass}: ', [
    'modelClass' => 'Bill Type',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('ecommerce', 'Bill Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('ecommerce', 'Modificar');
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="bill-type-modificar">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>


</div>