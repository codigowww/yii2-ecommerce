<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model codigowww\yii2ecommerce\models\BillType */

$this->title = Yii::t('ecommerce', 'Crear Bill Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('ecommerce', 'Bill Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="bill-type-crear">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>