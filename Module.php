<?php

namespace codigowww\yii2ecommerce;

use Yii;
/**
 * ecommerce module definition class
 */
class Module extends \yii\base\Module {

    public $requireLogin = false;
    public $currency = 'USD';
    public $billinfo = true;
    public $tax = 0;
    public $showLogin = true;

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'codigowww\yii2ecommerce\controllers';

    /**
     * {@inheritdoc}
     */
    public function init() {
        parent::init();

        $this->components = [
            'cart' => [
                'class' => 'codigowww\yii2ecommerce\components\Cart',
            ],
        ];
        $this->registerTranslations();
        // custom initialization code goes here
    }

    public function registerTranslations() {
        
        $i18n = Yii::$app->i18n;
        $i18n->translations['ecommerce/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@codigowww/yii2-ecommerce/messages',
            'fileMap' => [],
        ];
        
    }

}
