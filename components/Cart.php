<?php

namespace codigowww\yii2ecommerce\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidParamException;
use codigowww\yii2ecommerce\models\ProductInterface;
use codigowww\yii2ecommerce\models\Coupon;

/**
 * Class Cart provides basic cart functionality (adding, removing, clearing, listing items). You can extend this class and
 * override it in the application configuration to extend/customize the functionality
 *
 * @package yii2mod\cart
 */
class Cart extends Component {

    /**
     * @var string ProductInterface class name
     */
    const ITEM_PRODUCT = 'codigowww\yii2ecommerce\models\ProductInterface';

    public $session_key = 'cart';

    /**
     * @var array cart items
     */
    protected $items;
    protected $coupon;

    /**
     * @inheritdoc
     */
    public function init() {
        $this->clear(false);
        $this->load();
    }

    /**
     * @return object
     */
    protected function getSession() {
        return Yii::$app->get('session');
    }

    protected function load() {
        $cartData = [];
        if (false !== ($session = ($this->session->get($this->session_key, false)))) {
            $cartData = unserialize($session);
            $this->items = $cartData['items'];
            $this->coupon = $cartData['coupon'];
        }else{
            $this->items = [];
            $this->coupon = null;
        }
    }

    protected function save() {
        $sessionData = serialize(['items' => $this->getItems(), 'coupon' => $this->coupon]);
        $this->session->set($this->session_key, $sessionData);
    }

    /**
     * Delete all items from the cart
     *
     * @param bool $save
     *
     * @return $this
     */
    public function clear($save = true): self {
        $this->items = [];
        $save && $this->save();

        return $this;
    }

    /**
     * Add an item to the cart
     *
     * @param models\CartItemInterface $element
     * @param bool $save
     *
     * @return $this
     */
    public function add(ProductInterface $element, $quantity = 1, $plus = true, $save = true): self {
        $this->addItem($element, $quantity, $plus);
        $save && $this->save();
        return $this;
    }

    /**
     * @param \yii2mod\cart\models\CartItemInterface $item
     */
    protected function addItem(ProductInterface $item, $quantity = 1, $plus = true) {
        $uniqueId = $item->getUniqueId();
        $quantity = $plus ? (isset($this->items[$uniqueId]['quantity']) ? $this->items[$uniqueId]['quantity'] + $quantity : $quantity ) : $quantity;
        $this->items[$uniqueId] = ['quantity' => $quantity, 'item' => $item];
    }

    public function setCoupon($couponcode) {
        $coupon = Coupon::findOne(['code' => $couponcode]);
        if (!is_null($coupon)) {
            $this->coupon = $couponcode;
        }
    }

    public function getCouponCode() {
        return $this->coupon;
    }

    public function getCoupon() {
        return Coupon::findOne(['code' => $this->coupon]);
    }

    /**
     * Removes an item from the cart
     *
     * @param string $uniqueId
     * @param bool $save
     *
     * @throws \yii\base\InvalidParamException
     *
     * @return $this
     */
    public function remove($uniqueId, $save = true): self {
        if (!isset($this->items[$uniqueId])) {
            throw new InvalidParamException('Item not found');
        }
        unset($this->items[$uniqueId]);
        $save && $this->save();
        return $this;
    }

    /**
     * @param string $itemType If specified, only items of that type will be counted
     *
     * @return int
     */
    public function getCount($itemType = null): int {
        return count($this->getItems($itemType));
    }


    public function getQuantity($uniqueId) {
        if (!isset($this->items[$uniqueId]['quantity']))
            return 0;
        return $this->items[$uniqueId]['quantity'];
    }

    /**
     * Returns all items of a given type from the cart
     *
     * @param string $itemType One of self::ITEM_ constants
     *
     * @return ProductInterface[]
     */
    public function getItems($itemType = null): array {
        $items = $this->items;

        if (!is_null($itemType)) {
            $items = array_filter(
                    $items, function ($item) use ($itemType) {
                /* @var $item ProductInterface */
                return is_a($item['item'], $itemType);
            }
            );
        }

        return $items;
    }

    /**
     * Finds all items of type $itemType, sums the values of $attribute of all models and returns the sum.
     *
     * @param string $attribute
     * @param string|null $itemType
     *
     * @return int
     */
    public function getAttributeTotal($attribute, $itemType = null): int {
        $sum = 0;
        foreach ($this->getItems($itemType) as $item) {
            $model = $item['item'];
            $sum += $model->{$attribute};
        }

        return $sum;
    }

    public function getTotal($itemType = null) {
        $sum = 0;
        foreach ($this->getItems($itemType) as $item) {
            $model = $item['item'];
            $sum += $model->price * $item['quantity'];
        }
        
        return $sum;
    }

}
