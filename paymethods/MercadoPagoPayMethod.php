<?php

namespace codigowww\yii2ecommerce\paymethods;

use MercadoPago\SDK;
use MercadoPago\Preference;
use MercadoPago\Item;
use codigowww\yii2ecommerce\models\Bill;
use codigowww\yii2ecommerce\models\BillIpn;
use yii\web\BadRequestHttpException;
use Yii;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DefaultPayMethod
 *
 * @author maxiz
 */
class MercadoPagoPayMethod implements \codigowww\yii2ecommerce\models\PayMethodInterface {

    public static function getTestUser($payMethod) {
        $config = $payMethod->getConfig();


        if (!isset($config->access_token)) {
            return false;
        }
        SDK::setAccessToken($config->access_token);

        $body = array(
            "json_data" => array(
                "site_id" => "MLA"
            )
        );
        $result = \MercadoPago\SDK::post('/users/test_user', $body);
        die(var_dump($result));
    }

    /**
     * 
     * @param \codigowww\yii2ecommerce\models\Bill $bill
     * @param \codigowww\yii2ecommerce\models\Bill\PayMethod $payMethod
     */
    public static function sendBill($bill, $payMethod) {
        $config = $payMethod->getConfig();

        if (!isset($config->access_token)) {
            die("ACCESS TOKEN NOT SET");
        }


        SDK::setAccessToken($config->access_token);


        $preference = new Preference();

        $mpitems = [];

        foreach ($bill->billItems as $billitem) {
            $item = new Item();
            $item->id = $billitem->id;
            $item->title = $billitem->name;
            $item->quantity = $billitem->quantity;
            $item->unit_price = $billitem->price;
            $mpitems[] = $item;
        }

        $preference->items = $mpitems;

        $preference->auto_return = "all";
        /* $preference->payment_methods = array(
          "excluded_payment_types" => array(
          array("id" => "credit_card")
          ),
          "installments" => 12
          ); */


        $preference->external_reference = $bill->hash;
        $preference->notification_url = $payMethod->getIpnUrl();
        $preference->back_urls = array(
            "success" => $payMethod->getSuccessUrl($bill->hash),
            "failure" => $payMethod->getFailUrl($bill->hash),
            "pending" => $payMethod->getPendingUrl($bill->hash)
        );


        if ($bill->billInfo) {
            $billinfo = $bill->billInfo;
            $payer = new \MercadoPago\Payer();
            $payer->name = $billinfo->f_name;
            $payer->surname = $billinfo->l_name;
            $payer->email = trim($billinfo->email);
            $payer->phone = array(
                "number" => trim($billinfo->phone),
            );
            $preference->payer = $payer;
        }
        /*
         * 
         *     "expires": true,
          "expiration_date_from": "2016-02-01T12:00:00.000-04:00",
          "expiration_date_to": "2016-02-28T12:00:00.000-04:00"
         */
        $preference->save(); # Save the preference and send the HTTP Request to create
        if ($config->sandbox) {
            return $preference->sandbox_init_point;
        }
        return $preference->init_point;
    }

    public static function onReturn($params, $bill, $payMethod) {
        /* $config = $payMethod->getConfig();
          SDK::setClientId($config->client_id);
          SDK::setClientSecret($config->client_secret);

          $preference = SDK::get('/checkout/preferences/'.$params['preference_id']); */
    }

    public static function onIPN($payMethod) {
        $merchant_order = null;
        $config = $payMethod->getConfig();
        SDK::setAccessToken($config->access_token);



        $type = $_GET["type"];
        if ($_GET["topic"])
            $type = $_GET["topic"];
        else if ($_GET["type"])
            $type = $_GET["type"];

        if (is_null($type)) {
            throw new BadRequestHttpException("Petición Inválida");
        }

        switch ($type) {

            case "payment":
                $payment = \MercadoPago\Payment::find_by_id(Yii::$app->request->get("data_id"));
                Yii::info(print_r($payment, true));
                $merchant_order = \MercadoPago\MerchantOrder::find_by_id($payment->order->id);
                break;
            /* case "plan":
              $plan = \MercadoPago\Plan::find_by_id($_POST["id"]);
              break;
              case "subscription":
              $plan = \MercadoPago\Subscription::find_by_id($_POST["id"]);
              break;
              case "invoice":
              $plan = \MercadoPago\Invoice::find_by_id($_POST["id"]);
              break; */
            case "merchant_order":
                $merchant_order = \MercadoPago\MerchantOrder::find_by_id($_GET["id"]);
                break;
        }


        Yii::info(print_r($merchant_order, true));
        $bill_hash = $merchant_order->external_reference;

        
        $bill = Bill::getByHash($bill_hash);
        $billipn = new BillIpn();
        $billipn->bill_id = $bill->id;
        $billipn->payload = print_r($merchant_order, true);
        $billipn->created_at = date('Y-m-d H:i:s');
        $billipn->updated_at = date('Y-m-d H:i:s');
        $billipn->save();

        $cancel = false;

        $paid_amount = 0;

        if($merchant_order && $merchant_order->payments && is_array($merchant_order->payments)){
            foreach ($merchant_order->payments as $payment) {

                if ($payment->status == 'approved') {
                    $paid_amount += $payment->transaction_amount;
                }
                if ($payment->status == 'rejected' || $payment->status == 'cancelled') {
                    $cancel = true;
                }
            }
        }
  

        // If the payment's transaction amount is equal (or bigger) than the merchant_order's amount you can release your items
        if ($paid_amount >= $merchant_order->total_amount) {
            /* if (count($merchant_order->shipments) > 0) { // The merchant_order has shipments
              if ($merchant_order->shipments[0]->status == "ready_to_ship") {
              // print_r("Totally paid. Print the label and release your item.");
              }
              } else { // The merchant_order don't has any shipments
              //print_r("Totally paid. Release your item.");
              } */

            return ['hash' => $bill_hash, 'status' => Bill::STATUS_SUCCESS];
        } else {

            if ($cancel) {
                return ['hash' => $bill_hash, 'status' => Bill::STATUS_FAIL];
            } else {
                return ['hash' => $bill_hash, 'status' => Bill::STATUS_PENDING];
            }
        }
    }

}
