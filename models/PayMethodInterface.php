<?php

namespace codigowww\yii2ecommerce\models;

/**
 * All objects that can be added to the cart must implement this interface
 *
 * @package yii2mod\cart\models
 */
interface PayMethodInterface {


    public static function sendBill($bill,$payMethod);

    public static function onIPN($payMethod);


}
