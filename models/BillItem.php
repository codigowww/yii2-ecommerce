<?php

namespace codigowww\yii2ecommerce\models;

use Yii;

/**
 * This is the model class for table "ecommerce_bill_item".
 *
 * @property int $id
 * @property int $bill_id
 * @property string $name
 * @property int $quantity
 * @property string $price
 * @property string $details
 * @property int $status
 *
 * @property EcommerceBill $bill
 */
class BillItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ecommerce_bill_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bill_id', 'name', 'quantity', 'price', 'status'], 'required'],
            [['bill_id', 'ref_id', 'quantity', 'status'], 'integer'],
            [['price'], 'number'],
            [['details'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['bill_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::className(), 'targetAttribute' => ['bill_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('ecommerce', 'ID'),
            'bill_id' => Yii::t('ecommerce', 'Bill ID'),
            'name' => Yii::t('ecommerce', 'Name'),
            'quantity' => Yii::t('ecommerce', 'Quantity'),
            'price' => Yii::t('ecommerce', 'Price'),
            'details' => Yii::t('ecommerce', 'Details'),
            'status' => Yii::t('ecommerce', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(Bill::className(), ['id' => 'bill_id']);
    }
}
