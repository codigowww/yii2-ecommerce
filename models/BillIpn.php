<?php

namespace codigowww\yii2ecommerce\models;

use Yii;

/**
 * This is the model class for table "{{%ecommerce_bill_info}}".
 *
 * @property int $id
 * @property int $bill_id
 * @property DateTime $created_at
 * @property DateTime $updated_at
 * @property int $status
 *
 * @property EcommerceBill $bill
 */
class BillIpn extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'ecommerce_bill_ipn';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['bill_id', 'status'], 'integer'],
            [['payload'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['bill_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::className(), 'targetAttribute' => ['bill_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('ecommerce', 'ID'),
            'bill_id' => Yii::t('ecommerce', 'Bill ID'),
            'created_at' => Yii::t('ecommerce', 'Created At'),
            'updated_at' => Yii::t('ecommerce', 'Updated At'),
            'status' => Yii::t('ecommerce', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill() {
        return $this->hasOne(Bill::className(), ['id' => 'bill_id']);
    }

}
