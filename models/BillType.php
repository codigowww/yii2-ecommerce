<?php

namespace codigowww\yii2ecommerce\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "ecommerce_bill_type".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 *
 * @property EcommerceBill[] $ecommerceBills
 */
class BillType extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'ecommerce_bill_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['status'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('ecommerce', 'ID'),
            'name' => Yii::t('ecommerce', 'Name'),
            'status' => Yii::t('ecommerce', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEcommerceBills() {
        return $this->hasMany(EcommerceBill::className(), ['type_id' => 'id']);
    }

    public static function getArray() {

        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

}
