<?php

namespace codigowww\yii2ecommerce\models;

use Yii;

use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "ecommerce_coupon".
 *
 * @property int $id
 * @property string $code
 * @property int $quantity
 * @property int $reuse
 * @property int $status
 * @property string $currency
 * @property string $discount
 * @property int $discount_type
 * @property string $created_at
 * @property string $updated_at
 * @property string $begin_at
 * @property string $end_at
 *
 * @property EcommerceBill[] $ecommerceBills
 */
class Coupon extends \yii\db\ActiveRecord {

    const TYPE_PERCENT = 10;
    const TYPE_VALUE = 20;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'ecommerce_coupon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['quantity', 'reuse', 'status', 'discount_type'], 'integer'],
            [['discount'], 'number'],
            [['created_at', 'updated_at', 'begin_at', 'end_at'], 'safe'],
            [['code'], 'string', 'max' => 50],
            [['currency'], 'string', 'max' => 3],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('ecommerce', 'ID'),
            'code' => Yii::t('ecommerce', 'Code'),
            'quantity' => Yii::t('ecommerce', 'Quantity'),
            'reuse' => Yii::t('ecommerce', 'Reuse'),
            'status' => Yii::t('ecommerce', 'Status'),
            'currency' => Yii::t('ecommerce', 'Currency'),
            'discount' => Yii::t('ecommerce', 'Discount'),
            'discount_type' => Yii::t('ecommerce', 'Discount Type'),
            'created_at' => Yii::t('ecommerce', 'Created At'),
            'updated_at' => Yii::t('ecommerce', 'Updated At'),
            'begin_at' => Yii::t('ecommerce', 'Begin At'),
            'end_at' => Yii::t('ecommerce', 'End At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEcommerceBills() {
        return $this->hasMany(EcommerceBill::className(), ['coupon_id' => 'id']);
    }

    public function setCreatedAtTimestamp($timestamp = null) {
        if (is_null($timestamp))
            $timestamp = time();
        $this->created_at = date("Y-m-d H:i:s", $timestamp);
    }

    public function setUpdatedAtTimestamp($timestamp = null) {
        if (is_null($timestamp))
            $timestamp = time();
        $this->updated_at = date("Y-m-d H:i:s", $timestamp);
    }

    public function setBeginAtTimestamp($timestamp = null) {
        if (is_null($timestamp))
            $timestamp = time();
        $this->begin_at = date("Y-m-d H:i:s", $timestamp);
    }

    public function setEndAtTimestamp($timestamp = null) {
        if (is_null($timestamp))
            $timestamp = time();
        $this->end_at = date("Y-m-d H:i:s", $timestamp);
    }

    public function getUniqueId() {
        return $this->id;
    }

    public static function getArray() {

        return ArrayHelper::map(self::find()->all(), 'id', 'code');
    }

}
