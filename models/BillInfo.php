<?php

namespace codigowww\yii2ecommerce\models;

use Yii;

/**
 * This is the model class for table "{{%ecommerce_bill_info}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $f_name
 * @property string $l_name
 * @property string $address
 * @property string $address2
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $tax
 * @property string $email
 * @property int $bill_type_id
 * @property string $country
 * @property string $phone
 * @property DateTime $created_at
 * @property DateTime $updated_at
 * @property int $status
 *
 * @property EcommerceBill[] $ecommerceBills
 * @property EcommerceBillType $billType
 */
class BillInfo extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'ecommerce_bill_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['user_id', 'bill_type_id', 'status'], 'integer'],
            [['f_name', 'l_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['f_name', 'l_name', 'address', 'address2', 'city', 'state', 'tax_id', 'country', 'phone'], 'string', 'max' => 255],
            [['email',], 'string', 'max' => 128],
            [['zip'], 'string', 'max' => 16],
            [['bill_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BillType::className(), 'targetAttribute' => ['bill_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('ecommerce', 'ID'),
            'user_id' => Yii::t('ecommerce', 'User ID'),
            'f_name' => Yii::t('ecommerce', 'F Name'),
            'l_name' => Yii::t('ecommerce', 'L Name'),
            'address' => Yii::t('ecommerce', 'Address'),
            'address2' => Yii::t('ecommerce', 'Address2'),
            'city' => Yii::t('ecommerce', 'City'),
            'state' => Yii::t('ecommerce', 'State'),
            'zip' => Yii::t('ecommerce', 'Zip'),
            'tax_id' => Yii::t('ecommerce', 'Tax ID'),
            'bill_type_id' => Yii::t('ecommerce', 'Bill Type ID'),
            'country' => Yii::t('ecommerce', 'Country'),
            'phone' => Yii::t('ecommerce', 'Phone'),
            'created_at' => Yii::t('ecommerce', 'Created At'),
            'updated_at' => Yii::t('ecommerce', 'Updated At'),
            'status' => Yii::t('ecommerce', 'Status'),
            'email' => Yii::t('ecommerce', 'Email'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBills() {
        return $this->hasMany(Bill::className(), ['bill_info_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillType() {
        return $this->hasOne(BillType::className(), ['id' => 'bill_type_id']);
    }

    public function setCreatedAtTimestamp($timestamp = null) {
        
        if(is_null($timestamp)) $timestamp = time();
        $this->created_at = date("Y-m-d H:i:s", $timestamp);
    }

    public function setUpdatedAtTimestamp($timestamp = null) {
        
        if(is_null($timestamp)) $timestamp = time();
        $this->updated_at = date("Y-m-d H:i:s", $timestamp);
    }

}
