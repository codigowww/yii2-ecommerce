<?php

namespace codigowww\yii2ecommerce\models;

/**
 * All objects that can be added to the cart must implement this interface
 *
 * @package yii2mod\cart\models
 */
interface ProductInterface {

    /**
     * Returns the price for the product
     *
     * @return double
     */
    public function getPrice();

    /**
     * Returns the label for the cart item (displayed in cart etc)
     *
     * @return int|string
     */
    public function getLabel();

    public function getDescription();

    /**
     * Returns unique id to associate cart item with product
     *
     * @return int|string
     */
    public function getUniqueId();

    public function hasQuantity();
}
