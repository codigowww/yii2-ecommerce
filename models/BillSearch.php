<?php

namespace codigowww\yii2ecommerce\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use codigowww\yii2ecommerce\models\Bill;

/**
 * BillSearch represents the model behind the search form about `codigowww\yii2ecommerce\models\Bill`.
 */
class BillSearch extends Bill
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'type_id', 'coupon_id', 'payment_method_id', 'bill_info_id', 'status'], 'integer'],
            [['transaction', 'currency', 'shipping_info', 'note', 'created_at', 'updated_at', 'payment_date'], 'safe'],
            [['subtotal', 'shipping',  'total', 'refund','tax'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bill::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);

        $this->load($params);

        
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'type_id' => $this->type_id,
            'coupon_id' => $this->coupon_id,
            'payment_method_id' => $this->payment_method_id,
            'bill_info_id' => $this->bill_info_id,
            'subtotal' => $this->subtotal,
            'shipping' => $this->shipping,
            'tax' => $this->tax,
            'total' => $this->total,
            'refund' => $this->refund,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'payment_date' => $this->payment_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'transaction', $this->transaction])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'shipping_info', $this->shipping_info])
            ->andFilterWhere(['like', 'note', $this->note]);
        
        

        return $dataProvider;
    }
}
