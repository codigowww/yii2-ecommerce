<?php

namespace codigowww\yii2ecommerce\models;

use Yii;
use codigowww\yii2ecommerce\components\Cart;

/**
 * This is the model class for table "ecommerce_bill".
 *
 * @property int $id
 * @property int $user_id
 * @property int $type_id
 * @property int $coupon_id
 * @property int $payment_method_id
 * @property int $bill_info_id
 * @property string $transaction
 * @property string $currency
 * @property string $subtotal
 * @property string $shipping
 * @property string $tax
 * @property string $hash
 * @property string $total
 * @property string $refund
 * @property string $shipping_info
 * @property string $note
 * @property string $created_at
 * @property string $updated_at
 * @property string $payment_date
 * @property string $expire_at
 * @property int $status
 *
 * @property BillInfo $billInfo
 * @property Coupon $coupon
 * @property PayMethod $paymentMethod
 * @property BillType $type
 * @property BillItem[] $billItems
 */
class Bill extends \yii\db\ActiveRecord {

    const STATUS_CREATED = 0;
    const STATUS_PENDING = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAIL = 3;
    const EVENT_BILL_SUCCESS = 'bill-success';
    const EVENT_BILL_PENDING = 'bill-pending';
    const EVENT_BILL_CREATED = 'bill-created';
    const EVENT_BILL_FAIL = 'bill-fail';

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'ecommerce_bill';
    }

    public static function getStatus() {
        return [
            self::STATUS_CREATED,
            self::STATUS_PENDING,
            self::STATUS_SUCCESS,
            self::STATUS_FAIL
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['user_id', 'type_id', 'coupon_id', 'payment_method_id', 'bill_info_id', 'status'], 'integer'],
            [['created_at', 'status'], 'required'],
            [['payment_method_id'], 'required', 'on' => 'checkout'],
            [['subtotal', 'shipping', 'total', 'refund', 'tax'], 'number'],
            [['shipping_info', 'note'], 'string'],
            [['created_at', 'updated_at', 'payment_date', 'expire_at'], 'safe'],
            [['transaction', 'hash'], 'string', 'max' => 255],
            [['currency'], 'string', 'max' => 3],
            [['bill_info_id'], 'exist', 'skipOnError' => true, 'targetClass' => BillInfo::className(), 'targetAttribute' => ['bill_info_id' => 'id']],
            [['coupon_id'], 'exist', 'skipOnError' => true, 'targetClass' => Coupon::className(), 'targetAttribute' => ['coupon_id' => 'id']],
            [['payment_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PayMethod::className(), 'targetAttribute' => ['payment_method_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BillType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('ecommerce', 'ID'),
            'user_id' => Yii::t('ecommerce', 'User ID'),
            'type_id' => Yii::t('ecommerce', 'Type ID'),
            'hash' => Yii::t('ecommerce', 'Hash'),
            'coupon_id' => Yii::t('ecommerce', 'Coupon ID'),
            'payment_method_id' => Yii::t('ecommerce', 'Payment Method ID'),
            'bill_info_id' => Yii::t('ecommerce', 'Bill Info ID'),
            'transaction' => Yii::t('ecommerce', 'Transaction'),
            'currency' => Yii::t('ecommerce', 'Currency'),
            'subtotal' => Yii::t('ecommerce', 'Subtotal'),
            'shipping' => Yii::t('ecommerce', 'Shipping'),
            'tax' => Yii::t('ecommerce', 'Tax'),
            'total' => Yii::t('ecommerce', 'Total'),
            'refund' => Yii::t('ecommerce', 'Refund'),
            'shipping_info' => Yii::t('ecommerce', 'Shipping Info'),
            'note' => Yii::t('ecommerce', 'Note'),
            'created_at' => Yii::t('ecommerce', 'Created At'),
            'updated_at' => Yii::t('ecommerce', 'Updated At'),
            'expire_at' => Yii::t('ecommerce', 'Expire At'),
            'payment_date' => Yii::t('ecommerce', 'Payment Date'),
            'status' => Yii::t('ecommerce', 'Status'),
        ];
    }

    private function applyCoupon($coupon) {
        if (!is_null($this->coupon_id)) {
            //revert coupon
            if ($coupon->discount_type == Coupon::TYPE_PERCENT) {
                $this->subtotal = $this->subtotal + ($this->subtotal * (1 + $coupon->discount));
            } else if ($coupon->discount_type == Coupon::TYPE_VALUE) {
                $this->subtotal += $coupon->discount;
            }
        }
        if ($coupon->discount_type == Coupon::TYPE_PERCENT) {
            $this->subtotal = $this->subtotal - ($this->subtotal * $coupon->discount);
        } else if ($coupon->discount_type == Coupon::TYPE_VALUE) {
            $this->subtotal -= $coupon->discount;
        }
    }

    public function loadCart($cart) {
        $cart_items = $cart->getItems(Cart::ITEM_PRODUCT);




        foreach ($cart_items as $cart_item) {

            $model = $cart_item['item'];
            $billitem = new BillItem();
            $billitem->bill_id = $this->id;
            $billitem->name = $model->label;
            $billitem->price = $model->price;
            $billitem->quantity = $cart_item['quantity'];
            $billitem->ref_id = $model->id;
            $billitem->status = 0;
            if (!$billitem->save()) {
                die(print_r($billitem->errors));
                throw new \yii\base\ErrorException("Fail to save bill item");
            }
        }
        $this->subtotal = $cart->getTotal(Cart::ITEM_PRODUCT);
        $coupon = $cart->coupon;
        if (!is_null($coupon)) {
            $this->coupon_id = $coupon->id;
        }

        $cart->clear();
    }

    public function calculateSubTotal() {
        $this->subtotal = 0;
        foreach ($this->billItems as $billitem) {
            $this->subtotal += $billitem->price * $billitem->quantity;
        }
        $coupon = $this->coupon;
        if (!is_null($coupon))
            $this->applyCoupon($coupon);
    }

    public function calculateTax() {
        $ecommerce = Yii::$app->getModule('ecommerce');
        if ($ecommerce->tax && $ecommerce->tax > 0) {
            $this->tax = $this->subtotal * $ecommerce->tax;
        }
    }

    public function calculate() {
        $this->calculateSubTotal();
        $this->calculateTax();
        //$this->calculateShipping();
        $this->calculateTotal();
    }

    public function calculateTotal() {
        $this->total = $this->subtotal + $this->shipping + $this->tax;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillInfo() {
        return $this->hasOne(BillInfo::className(), ['id' => 'bill_info_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoupon() {
        return $this->hasOne(Coupon::className(), ['id' => 'coupon_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethod() {
        return $this->hasOne(PayMethod::className(), ['id' => 'payment_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType() {
        return $this->hasOne(BillType::className(), ['id' => 'type_id']);
    }

    public function getStatusText() {

        switch ($this->status) {
            case self::STATUS_CREATED: return Yii::t('ecommerce', 'Created');
            case self::STATUS_PENDING: return Yii::t('ecommerce', 'Pending');
            case self::STATUS_SUCCESS: return Yii::t('ecommerce', 'Success');
            case self::STATUS_FAIL: return Yii::t('ecommerce', 'Fail');
            default: return Yii::t('ecommerce', '?');
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillItems() {
        return $this->hasMany(BillItem::className(), ['bill_id' => 'id']);
    }

    public function setCreatedAtTimestamp($timestamp = null) {
        if (is_null($timestamp))
            $timestamp = time();
        $this->created_at = date("Y-m-d H:i:s", $timestamp);
    }

    public function setUpdatedAtTimestamp($timestamp = null) {

        if (is_null($timestamp))
            $timestamp = time();
        $this->updated_at = date("Y-m-d H:i:s", $timestamp);
    }

    public function setPaymentDateTimestamp($timestamp = null) {

        if (is_null($timestamp))
            $timestamp = time();
        $this->payment_date = date("Y-m-d H:i:s", $timestamp);
    }

    public static function getByHash($hash) {
        return Bill::findOne(['hash' => $hash]);
    }

    public function getIdHash() {
        return md5($str);
    }

    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        
        if ($insert) {
            
            $str=rand(); 
            $this->hash = md5(Yii::$app->id . $this->currency .$str. time() .$this->created_at);
        }
        return true;
    }

}
