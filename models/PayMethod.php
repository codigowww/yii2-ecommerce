<?php

namespace codigowww\yii2ecommerce\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "ecommerce_pay_method".
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property string $class
 * @property int $status
 *
 * @property Bill[] $ecommerceBills
 */
class PayMethod extends \yii\db\ActiveRecord {

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'ecommerce_pay_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'title', 'status', 'class'], 'required'],
            [['description', 'config'], 'string'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['title', 'class'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('ecommerce', 'ID'),
            'name' => Yii::t('ecommerce', 'Name'),
            'title' => Yii::t('ecommerce', 'Title'),
            'config' => Yii::t('ecommerce', 'Config'),
            'description' => Yii::t('ecommerce', 'Description'),
            'status' => Yii::t('ecommerce', 'Status'),
            'class' => Yii::t('ecommerce', 'Class'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /* public function getBills()
      {
      return $this->hasMany(EcommerceBill::className(), ['payment_method_id' => 'id']);
      } */

    public function getProvider() {
        return $this->class;
    }

    public function getConfig() {
        return json_decode($this->config);
    }

    public function setConfig($config) {
        $this->config = json_encode($config);
    }

    public function getIpnUrl() {
        return Url::toRoute(['/ecommerce/bill/ipn', 'pm' => $this->id], true);
    }

    public function getSuccessUrl($hash) {
        return Url::toRoute(['/ecommerce/bill/return', 'hash' => $hash, 'method' => $this->id, 'status' => 'success'], true);
    }

    public function getFailUrl($hash) {
        return Url::toRoute(['/ecommerce/bill/return', 'hash' => $hash, 'method' => $this->id, 'status' => 'fail'], true);
    }

    public function getPendingUrl($hash) {
        return Url::toRoute(['/ecommerce/bill/return', 'hash' => $hash, 'method' => $this->id, 'status' => 'pending'], true);
    }
    
    public static function getArray(){
        
        return ArrayHelper::map(self::find()->all(),'id','name');
    }
}
